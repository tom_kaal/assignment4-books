/* jslint node: true */
(function () {
    "use strict";
    /**
     * Module dependencies.
     */
    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        schemaName,
        modelName;
    /**
     * Creates a new mongoose schema.
     * -----
     * Instructions, hints and questions
     * - Instruction: create a Schema with the following properties
     *   - title
     *   - author
     *   - description
     *   - modificationDate
     * - The properties are defined as follow:
     *   - title, author and description are strings
     *   - modificationDate is a date
     *   - a title of the book is unique
     *   - title and author are required
     *   - the default value for date is now
     *
     * - Question: What are the differences between a 'Schema Type' and a JSON object? Use the references to motivate your answer.
     * The schema type sort of describes how the JSON object should look like.
     * - Question: What is the function of the 'collection' property?
     * Mongoose by default produces a collection name by passing the model name to the utils.toCollectionName method. This method pluralizes the name. This should be here when you want to rename your collection.
     * - Question: Suppose you have a model for 'Person'. What would be the default collection name?
     * People
     * @class Schema/Book
     * @returns Schema object
     * @see http://www.json.org/
     * @see http://mongoosejs.com/docs/schematypes.html
     * @see http://mongoosejs.com/docs/guide.html#collection
     */
    schemaName = new Schema({
        title: {
            type: String,
            required: true,
            unique: true
        },
        author: {
            type: String,
            require: true
        },
        description: {
            type: String
        },
        modificationDate: {
            type: Date,
            "default": Date.now
        }
    }, {
        collection: 'books'
    });
    /**
     * Custom validator
     * -------
     * Instructions, hints and questions.
     *
     * In Mongoose you can define custom validators.
     * If the value does not fit the the definition, an error is returned.

     * - Instruction: Add a validator for title. A title must have a length of at least 8 characters.
     * - Question: There are four locations of validations, each for a specific type of validation. Give for each validation an example and describe why that location is the best location for that specific type of validation.
     *   1. Database (technical constraints, primary key)
     *   on the database
     *   1. Schema (simple business rules)
     *   on the databas
     *   1. Application (complex business rules)
     *   could be on both sites
     *   1. Client-side (form validation)
     *   on the client site
     * @class Validator/Book/title
     * @returns true or false. In case of ```false```, a message 'Invalid title' is returned as well.
     * @see http://mongoosejs.com/docs/validation.html
     */

    /**
     * Instructions, hints and questions.
     * - Instruction: Create a model for the defined schema.
     * - Question: What are the differences between a 'Model' and a 'Schema Type'? Use the references to motivate your answer.
     * @class Model/Book
     * @see http://mongoosejs.com/docs/models.html
     */
    schemaName.path('title').validate(function (val) {
        return (val !== undefined && val !== null && val.length >= 0);
    }, 'invalid title');

    modelName = "Book";
    mongoose.model(modelName, schemaName);

}());