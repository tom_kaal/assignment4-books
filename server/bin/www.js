/*jslint node: true, devel: true*/
"use strict";

var app = require('../server.js'); //require our app
var server = app.listen(app.get('port'), function () {
    console.log('express server listening on port ' + server.address().port);
});