/*jslint node: true*/
"use strict";
/**
 * Module dependencies.
 * @type {exports}
 */
var fs = require('fs'),
    http = require('http'),
    express = require('express'),
    bodyParser = require("body-parser"),
    env,
    config,
    mongoose,
    models_path,
    model_files,
    app,
    route_path,
    route_files;


/**
 * Load configuration
 * @type {*|string}
 */
env = process.env.NODE_ENV || 'development';
config = require('./config/config.js')[env];
/**
 * Bootstrap db connection
 * @type {exports}
 */
mongoose = require('mongoose');
mongoose.connect(config.db);

/**
 * debugging
 */
console.log(config.debug);
mongoose.connection.on('error', function (err) {
    console.error('mongodb error: %s', err);
});
mongoose.set('debug', config.debug);
/**
 * Bootstrap models
 * @type {string}
 */
models_path = __dirname + '/app/models';
model_files = fs.readdirSync(models_path);
model_files.forEach(function (file) {
    require(models_path + '/' + file);
});
/**
 * Use express
 * @type {*}
 */
app = express();
/**
 * Express settings
 */
app.set('port', process.env.PORT || config.port);
/**
 * Express middleware
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
/** TODO: Add middleware to parse JSON input; @see https://github.com/expressjs/body-parser#bodyparserjsonoptions */
/** TODO: Add middleware to parse url-encoded input; @see https://github.com/expressjs/body-parser#bodyparserurlencodedoptions */

/**
 * Middleware to enable logging
 */
if (config.debug) {
    app.use(function (req, res, next) {
        console.log('%s %s %s', req.method, req.url, req.path);
        next();
    });
}
/**
 * Middleware to serve static page
 */
app.use(express.static(__dirname + '/../client'));
/**
 * Bootstrap routes
 * @type {string}
 */
route_path = __dirname + '/routes';
route_files = fs.readdirSync(route_path);
route_files.forEach(function (file) {
    var route = require(route_path + '/' + file);
    app.use('/api', route);
});
/**
 * Middleware to catch all unmatched routes
 */
app.all('*', function (req, res) {
    res.send(404);
});



module.exports = app;