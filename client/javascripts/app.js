/*global angular, BooklistCtrl, BookDetailCtrl */
/*jslint node: true */
/*jshint strict:false */

"use strict";

/**
 * @see http://docs.angularjs.org/guide/concepts
 */
var myApp = angular.module('myApp', ['myApp.services', 'ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {

        // Get all books
        $routeProvider.when('/books', {
            templateUrl: 'partials/book-list.html',
            controller: BooklistCtrl
        });


        // Operations on 1 book
        $routeProvider.when('/books/:_id', {
            templateUrl: 'partials/book-details.html',
            controller: BookDetailCtrl
        });
        // When no valid route is provided
        $routeProvider.otherwise({
            redirectTo: "/books"
        });
    }]);